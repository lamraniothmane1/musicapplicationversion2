# **Overview of the Project:**

This projet is an Android Application that allows users to search their artists and get their top albums but a cool feature that saves those albums locally. The user can manage his saved albums.


# Technologies used:

- Kotlin
- Component architecture
- Clean Architecture MVVM
- Dagger2
- LiveData
- Espresso and JUnit for unit and UI Testing
- Room
- Navigation component
- Custom views

# Screenshots:

**Saved albums screen**

![Unknown2](/uploads/689ec9dfe858d6f85b362dedcf280ac4/Unknown2.jpg)

**Search Artist screen**

![Unknown3](/uploads/b9e32e28131f56c0084fbb5108d21e08/Unknown3.jpg)

**Search Albums scren**

![Unknown4](/uploads/0f336029afe09239a5061fb679761b9f/Unknown4.jpg)

**Album details screen**

![Unknown5](/uploads/8bdbd810e9d6528de50309a1cf4cc6a0/Unknown5.jpg)

