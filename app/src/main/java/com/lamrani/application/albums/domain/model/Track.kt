package com.lamrani.application.albums.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Lamrani Othmane on 2020-07-11.
 */
@Entity(tableName = "track_table")
data class Track(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int? = null,
    @ColumnInfo(name = "artist")
    val artist: String,
    @ColumnInfo(name = "duration")
    val duration: String,
    @ColumnInfo(name = "name")
    val name: String,
    var trackAlbumId: Int? = null
)