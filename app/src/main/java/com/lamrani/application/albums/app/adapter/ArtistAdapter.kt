package com.lamrani.application.albums.app.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamrani.application.R
import com.lamrani.application.albums.domain.model.Artist
import com.lamrani.application.core.app.adapter.MainAdapter
import kotlinx.android.synthetic.main.item_artist.view.*
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-10.
 */
class ArtistAdapter @Inject constructor() :  MainAdapter<Artist>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_artist, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        (holder as ViewHolder).onBind(position)
    }


    /**
     * View holder
     */
    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(position: Int) {
            val item = getItemAtPosition(position)
            itemView.artistName.text = item.name

            itemView.setOnClickListener {
                Log.i("Item clicked", "At position: $position")
                onClickListener?.invoke(item)
            }
        }

    }

    /**
     * Filtering the data set using the name of the Artist
     */
    override fun compare(query: String, data: Artist): Boolean {
        return data.name.contains(query, true)
    }

}