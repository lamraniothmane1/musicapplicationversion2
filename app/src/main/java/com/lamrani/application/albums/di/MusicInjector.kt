package com.lamrani.application.albums.di

import com.lamrani.application.albums.app.fragment.SearchAlbumDetailsFragment
import com.lamrani.application.albums.app.fragment.SearchAlbumsListFragment
import com.lamrani.application.albums.app.fragment.SearchArtistListFragment
import com.lamrani.application.albums.app.fragment.UsersAlbumListFragment
import com.lamrani.application.core.App

object MusicInjector {

    private fun buildComponent(): MusicComponent {
        return DaggerMusicComponent.builder()
            .appComponent(App.appComponent)
            .musicModule(MusicModule())
            .build()
    }

    fun inject(searchArtistListFragment: SearchArtistListFragment) {
        buildComponent().inject(searchArtistListFragment)
    }

    fun inject(searchAlbumsListFragment: SearchAlbumsListFragment) {
        buildComponent().inject(searchAlbumsListFragment)
    }

    fun inject(usersAlbumListFragment: UsersAlbumListFragment) {
        buildComponent().inject(usersAlbumListFragment)
    }

    fun inject(searchAlbumDetailsFragment: SearchAlbumDetailsFragment) {
        buildComponent().inject(searchAlbumDetailsFragment)
    }

}
