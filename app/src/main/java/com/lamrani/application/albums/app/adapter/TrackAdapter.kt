package com.lamrani.application.albums.app.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lamrani.application.R
import com.lamrani.application.albums.domain.model.Track
import com.lamrani.application.core.app.adapter.MainAdapter
import com.lamrani.application.core.utils.Utils
import kotlinx.android.synthetic.main.item_track.view.*
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-10.
 */
class TrackAdapter @Inject constructor() :  MainAdapter<Track>(){

    @Inject
    lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_track, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        (holder as ViewHolder).onBind(position)
    }


    /**
     * View holder
     */
    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(position: Int) {
            val item = getItemAtPosition(position)
            itemView.trackName.text = item.name
            itemView.tvDuration.text = Utils.secondsToTime(item.duration)

            itemView.setOnClickListener {
                Log.i("Item clicked", "At position: $position")
                onClickListener?.invoke(item)
            }
        }

    }

    /**
     * Filtering the data set using the name of the Album
     */
    override fun compare(query: String, data: Track): Boolean {
        return data.name.contains(query, true)
    }

}