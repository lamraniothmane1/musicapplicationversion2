package com.lamrani.application.albums.domain.usecase

import com.lamrani.application.albums.data.repository.MusicRepository
import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.core.app.callback.BaseCallback
import com.lamrani.application.core.app.wrapper.MessageWrapper
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-11.
 */
class GetAlbumDetailsUseCase @Inject constructor() {

    @Inject
    lateinit var musicRepository: MusicRepository

    interface Callback : BaseCallback {
        fun onSuccess(album: Album)
    }

    fun execute(artistName: String, albumName: String, callback: Callback){
        musicRepository.getAlbumDetails(
            artistName = artistName,
            albumName = albumName,
            callback = object : MusicRepository.GetAlbumDetailsCallback{

                override fun onSuccess(album: Album) {
                    callback.onSuccess(album)
                }

                override fun onTechnicalError(error: MessageWrapper) {
                    callback.onTechnicalError(error)
                }

                override fun onNetworkUnavailable() {
                    callback.onNetworkUnavailable()
                }

            }
        )
    }

}