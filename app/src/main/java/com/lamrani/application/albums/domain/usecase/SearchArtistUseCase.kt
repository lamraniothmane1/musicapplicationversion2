package com.lamrani.application.albums.domain.usecase

import com.lamrani.application.albums.data.repository.MusicRepository
import com.lamrani.application.albums.domain.model.Artist
import com.lamrani.application.core.app.callback.BaseCallback
import com.lamrani.application.core.app.wrapper.MessageWrapper
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-09.
 */
class SearchArtistUseCase @Inject constructor() {

    @Inject
    lateinit var musicRepository: MusicRepository

    interface Callback : BaseCallback {
        fun onSuccess(artists: ArrayList<Artist>)
    }

    fun execute(artistName: String, page: Int, callback: Callback){
        musicRepository.getArtists(
            artistName = artistName,
            page = page,
            callback = object : MusicRepository.GetArtistsListCallback{
                override fun onSuccess(artists: ArrayList<Artist>) {
                    callback.onSuccess(artists)
                }

                override fun onTechnicalError(error: MessageWrapper) {
                    callback.onTechnicalError(error)
                }

                override fun onNetworkUnavailable() {
                    callback.onNetworkUnavailable()
                }

            }
        )
    }

}