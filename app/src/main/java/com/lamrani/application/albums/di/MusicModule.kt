package com.lamrani.application.albums.di

import android.content.Context
import com.lamrani.application.albums.data.service.MusicApiService
import com.lamrani.application.albums.data.room.dao.MusicDao
import com.lamrani.application.albums.data.room.database.MusicDatabase
import com.lamrani.application.core.di.scopes.ScopeFragment
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
open class MusicModule {

    @Provides
    @ScopeFragment
    open fun providesMusicService(retrofit: Retrofit) : MusicApiService {
        return retrofit.create(MusicApiService::class.java)
    }


    @Provides
    @ScopeFragment
    open fun providesAlbumDao(context: Context) : MusicDao{
        return MusicDatabase.getDatabaseInstance(context).albumDAO()
    }

}