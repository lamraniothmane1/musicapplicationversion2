package com.lamrani.application.albums.data.json


import com.google.gson.annotations.SerializedName

data class SearchArtistJSON(
    @SerializedName("results")
    val results: Results?
) {
    data class Results(
        @SerializedName("artistmatches")
        val artistmatches: Artistmatches?,
        @SerializedName("@attr")
        val attr: Attr?,
        @SerializedName("opensearch:itemsPerPage")
        val opensearchItemsPerPage: String?,
        @SerializedName("opensearch:Query")
        val opensearchQuery: OpensearchQuery?,
        @SerializedName("opensearch:startIndex")
        val opensearchStartIndex: String?,
        @SerializedName("opensearch:totalResults")
        val opensearchTotalResults: String?
    ) {
        data class Artistmatches(
            @SerializedName("artist")
            val artist: List<Artist?>?
        ) {
            data class Artist(
                @SerializedName("image")
                val image: List<Image?>?,
                @SerializedName("listeners")
                val listeners: String?,
                @SerializedName("mbid")
                val mbid: String?,
                @SerializedName("name")
                val name: String?,
                @SerializedName("streamable")
                val streamable: String?,
                @SerializedName("url")
                val url: String?
            ) {
                data class Image(
                    @SerializedName("size")
                    val size: String?,
                    @SerializedName("#text")
                    val text: String?
                )
            }
        }

        data class Attr(
            @SerializedName("for")
            val forX: String?
        )

        data class OpensearchQuery(
            @SerializedName("role")
            val role: String?,
            @SerializedName("searchTerms")
            val searchTerms: String?,
            @SerializedName("startPage")
            val startPage: String?,
            @SerializedName("#text")
            val text: String?
        )
    }
}