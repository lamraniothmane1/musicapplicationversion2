package com.lamrani.application.albums.domain.mapper

import com.lamrani.application.albums.data.json.GetAlbumInfoJSON
import com.lamrani.application.albums.data.json.GetTopAlbumsJSON
import com.lamrani.application.albums.data.json.SearchArtistJSON
import com.lamrani.application.albums.data.room.entity.AlbumEntity
import com.lamrani.application.albums.data.room.entity.AlbumWithTracks
import com.lamrani.application.albums.domain.model.*
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-09.
 */
class MusicMapper @Inject constructor() {

    fun toArtistList(searchArtistJSON: SearchArtistJSON) : ArrayList<Artist>{
        return ArrayList<Artist>().apply{
            searchArtistJSON.results?.artistmatches?.artist?.let {
                for(item in it){
                    item?.let { artist ->
                        add(toArtist(artist))
                    }
                }
            }

        }
    }

    fun toArtist(artistJSON: SearchArtistJSON.Results.Artistmatches.Artist) : Artist {
        return artistJSON.let {
            Artist(
                mbid = it.mbid ?: "",
                name = it.name ?: "",
                image = it.image?.get(0)?.text ?: "",
                listeners = it.listeners ?: "",
                url = it.url ?: "",
                streamable = it.streamable ?: ""
            )
        }
    }

    fun toAlbumList(getTopAlbumsJSON: GetTopAlbumsJSON) : ArrayList<Album> {
        return ArrayList<Album>().apply{
            getTopAlbumsJSON.topalbums?.album?.let {
                for(item in it){
                    item?.let { album ->
                        add(toAlbum(album))
                    }
                }
            }
        }
    }

    fun toAlbum(albumJSON: GetTopAlbumsJSON.Topalbums.Album) : Album {
        return albumJSON.let {
            Album(
                mbid = it.mbid ?: "",
                name = it.name ?: "",
                artist = it.artist?.name ?: "",
                image = it.image?.get(0)?.text ?: "",
                playcount = "${it.playcount}",
                url = it.url ?: ""
            )
        }
    }

    fun toAlbum(getAlbumInfoJSON: GetAlbumInfoJSON) : Album? {
        return getAlbumInfoJSON.album?.let {
            Album(
                mbid = it.mbid ?: "",
                name = it.name ?: "",
                artist = it.artist ?: "",
                image = it.image?.get(0)?.text ?: "",
                playcount = it.playcount ?: "",
                listeners = it.listeners ?: "",
                url = it.url ?: ""
            ).apply {
                // getting the tracks
                it.tracks?.track?.let { items ->
                    for(item in items){
                        item?.let { track ->
                            tracks.add(toTrack(track))
                        }
                    }
                }

            }
        }
    }

    fun toTrack(track: GetAlbumInfoJSON.Album.Tracks.Track) : Track{
        return track.let {
            Track(
                name = it.name ?: "",
                duration = it.duration ?: "",
                artist = it.artist?.name ?: ""
            )
        }
    }

    fun toAlbumEntity(album: Album?) : AlbumEntity? {
        return album?.let {
            AlbumEntity(
                mbid = it.mbid,
                name = it.name,
                artist = it.artist,
                image = it.image,
                playcount = it.playcount,
                listeners = it.listeners,
                url = it.url
                //tracks = it.tracks
            )
        }
    }


    fun toAlbum(albumTrack: AlbumWithTracks?) : Album? {
        return albumTrack?.let {
            Album(
                mbid = it.album.mbid,
                name = it.album.name,
                artist = it.album.artist,
                image = it.album.image,
                playcount = it.album.playcount,
                listeners = it.album.listeners,
                url = it.album.url,
                tracks = it.tracks as ArrayList<Track>
            )
        }
    }

}