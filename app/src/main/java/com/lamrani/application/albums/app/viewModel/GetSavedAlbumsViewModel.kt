package com.lamrani.application.albums.app.viewModel

import androidx.lifecycle.MutableLiveData
import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.albums.domain.usecase.GetAlbumsFromDatabaseUsecase
import com.lamrani.application.core.app.callback.BaseViewModel
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-10.
 */
class GetSavedAlbumsViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var searchAlbumsUseCase: GetAlbumsFromDatabaseUsecase

    var albumsLD = MutableLiveData<ArrayList<Album>>()

    fun getSavedAlbums(){
        albumsLD.value = searchAlbumsUseCase.execute()
    }

}