package com.lamrani.application.albums.app.viewModel

import androidx.lifecycle.MutableLiveData
import com.lamrani.application.albums.domain.model.Artist
import com.lamrani.application.albums.domain.usecase.SearchArtistUseCase
import com.lamrani.application.core.app.callback.BaseViewModel
import com.lamrani.application.core.app.wrapper.MessageWrapper
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-09.
 */
class SearchArtistViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var searchArtistUseCase: SearchArtistUseCase

    var artistsLD = MutableLiveData<ArrayList<Artist>>()


    fun getArtists(artist: String?, page: Int){
        artist?.let {
            searchArtistUseCase.execute(
                artistName = it,
                page = page,
                callback = object : SearchArtistUseCase.Callback{
                    override fun onSuccess(artists: ArrayList<Artist>) {
                        artistsLD.value = artists
                    }

                    override fun onTechnicalError(error: MessageWrapper) {
                        this@SearchArtistViewModel.onBaseTechnicalError(error)
                    }

                    override fun onNetworkUnavailable() {
                        this@SearchArtistViewModel.onBaseNetworkUnavailable()
                    }

                })
        }
    }

}