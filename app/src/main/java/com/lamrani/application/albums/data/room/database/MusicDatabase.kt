package com.lamrani.application.albums.data.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.lamrani.application.albums.data.room.dao.MusicDao
import com.lamrani.application.albums.data.room.entity.AlbumEntity
import com.lamrani.application.albums.domain.model.Track

/**
 * Created by Lamrani Othmane on 2020-07-11.
 */
@Database(entities = [AlbumEntity::class, Track::class], version = 13)
abstract class MusicDatabase : RoomDatabase() {

    abstract fun albumDAO(): MusicDao

    companion object {

        @Volatile
        private var INSTANCE: MusicDatabase? = null

        /**
         * synchronized ==> ONE MAIN THREAD CAN ACCESS THIS METHOD
         */
        fun getDatabaseInstance(context: Context): MusicDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        MusicDatabase::class.java,
                        "music_database"
                    )
                    .fallbackToDestructiveMigration() // delete all the database once we update the database version
                    .allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }

}