package com.lamrani.application.albums.domain.usecase

import com.lamrani.application.albums.data.repository.MusicDatabaseRepository
import com.lamrani.application.albums.domain.mapper.MusicMapper
import com.lamrani.application.albums.domain.model.Album
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-11.
 */

class AddAlbumToDatabaseUsecase @Inject constructor() {

    @Inject
    lateinit var databaseRepository: MusicDatabaseRepository
    @Inject
    lateinit var musicMapper: MusicMapper

    fun execute(album: Album) : Long
            = musicMapper.toAlbumEntity(album)?.let { databaseRepository.addAlbum(it, album.tracks) } ?: -1

}