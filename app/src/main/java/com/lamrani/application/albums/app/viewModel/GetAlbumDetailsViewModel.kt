package com.lamrani.application.albums.app.viewModel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.albums.domain.usecase.AddAlbumToDatabaseUsecase
import com.lamrani.application.albums.domain.usecase.GetAlbumDetailsFromDatabaseUsecase
import com.lamrani.application.albums.domain.usecase.GetAlbumDetailsUseCase
import com.lamrani.application.albums.domain.usecase.RemoveAlbumFromDatabaseUsecase
import com.lamrani.application.core.app.callback.BaseViewModel
import com.lamrani.application.core.app.wrapper.MessageType
import com.lamrani.application.core.app.wrapper.MessageWrapper
import com.lamrani.application.core.utils.Utils
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-11.
 */
class GetAlbumDetailsViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var getAlbumDetailsUseCase: GetAlbumDetailsUseCase
    @Inject
    lateinit var addAlbumToDatabaseUsecase: AddAlbumToDatabaseUsecase
    @Inject
    lateinit var removeAlbumFromDatabaseUsecase: RemoveAlbumFromDatabaseUsecase
    @Inject
    lateinit var getAlbumDetailsFromDatabaseUsecase: GetAlbumDetailsFromDatabaseUsecase
    @Inject
    lateinit var mContext: Context


    var albumLD = MutableLiveData<Album>()
    var localAlbumLD = MutableLiveData<Album>()
    var addedAlbumLD = MutableLiveData<Long>()
    var removedAlbumLD = MutableLiveData<Int>()


    fun getAlbumDetails(artistName: String?, albumName: String?){
        if(artistName.isNullOrEmpty() || albumName.isNullOrEmpty()){
            this.onBaseTechnicalError(
                MessageWrapper(
                    message = "album or artist are null",
                    messageType = MessageType.ERROR
                )
            )
        }
        else{
            if(Utils.isNetworkAvailable(mContext)){
                getAlbumDetailsUseCase.execute(
                    artistName = artistName,
                    albumName = albumName,
                    callback = object : GetAlbumDetailsUseCase.Callback{

                        override fun onSuccess(album: Album) {
                            albumLD.value = album
                        }

                        override fun onTechnicalError(error: MessageWrapper) {
                            this@GetAlbumDetailsViewModel.onBaseTechnicalError(error)
                        }

                        override fun onNetworkUnavailable() {
                            this@GetAlbumDetailsViewModel.onBaseNetworkUnavailable()
                        }

                    }
                )
            } else {
                albumLD.value = getAlbumDetailsFromDatabaseUsecase.execute(artistName, albumName)
            }

        }
    }


    fun addAlbum(album: Album){
        addedAlbumLD.value = addAlbumToDatabaseUsecase.execute(album)
    }

    fun removeAlbum(album: Album){
        removedAlbumLD.value = removeAlbumFromDatabaseUsecase.execute(album)
    }

    fun findLocalAlbum(artistName: String?, albumName: String?) {
        if (artistName==null || albumName==null) {
            this.onBaseTechnicalError(
                MessageWrapper(
                    message = "album or artist are null",
                    messageType = MessageType.ERROR
                )
            )
        } else {
            localAlbumLD.value = getAlbumDetailsFromDatabaseUsecase.execute(artistName, albumName)
        }
    }

}