package com.lamrani.application.albums.app.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lamrani.application.R
import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.core.app.adapter.MainAdapter
import kotlinx.android.synthetic.main.item_album.view.*
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-10.
 */
class AlbumAdapter @Inject constructor() :  MainAdapter<Album>(){

    @Inject
    lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_album, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        (holder as ViewHolder).onBind(position)
    }


    /**
     * View holder
     */
    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(position: Int) {
            val item = getItemAtPosition(position)
            itemView.tvAlbumName.text = item.name

            Glide.with(mContext)
                .load(item.image)
                .circleCrop()
                .into(itemView.iv_image)

            itemView.setOnClickListener {
                Log.i("Item clicked", "At position: $position")
                onClickListener?.invoke(item)
            }
        }

    }

    /**
     * Filtering the data set using the name of the Album
     */
    override fun compare(query: String, data: Album): Boolean {
        return data.name.contains(query, true)
    }

}