package com.lamrani.application.albums.data.service

import com.lamrani.application.albums.data.json.GetAlbumInfoJSON
import com.lamrani.application.albums.data.json.GetTopAlbumsJSON
import com.lamrani.application.albums.data.json.SearchArtistJSON
import com.lamrani.application.core.data.retrofit.API_CONFIGURATION
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Lamrani Othmane on 2020-07-09.
 */
interface MusicApiService {

    @GET("?method=artist.search&format=json&api_key=${API_CONFIGURATION.API_KEY}")
    fun searchArtist(
        @Query("artist") artist: String,
        @Query("page") page: Int
    ): Call<SearchArtistJSON>

    @GET("?method=artist.gettopalbums&format=json&api_key=${API_CONFIGURATION.API_KEY}")
    fun searchAlbums(
        @Query("artist") artist: String,
        @Query("page") page: Int
    ): Call<GetTopAlbumsJSON>

    @GET("?method=album.getinfo&format=json&api_key=${API_CONFIGURATION.API_KEY}")
    fun getAlbumDetails(
        @Query("artist") artist: String,
        @Query("album") album: String
    ): Call<GetAlbumInfoJSON>


}