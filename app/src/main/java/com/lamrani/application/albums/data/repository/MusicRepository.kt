package com.lamrani.application.albums.data.repository

import android.content.Context
import com.lamrani.application.albums.data.service.MusicApiService
import com.lamrani.application.albums.data.json.GetAlbumInfoJSON
import com.lamrani.application.albums.data.json.GetTopAlbumsJSON
import com.lamrani.application.albums.data.json.SearchArtistJSON
import com.lamrani.application.albums.domain.mapper.MusicMapper
import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.albums.domain.model.Artist
import com.lamrani.application.core.app.callback.BaseCallback
import com.lamrani.application.core.app.wrapper.MessageWrapper
import com.lamrani.application.core.data.retrofit.GenericRequestHandler
import retrofit2.Call
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-09.
 */
class MusicRepository @Inject constructor() {

    @Inject
    lateinit var musicApiService: MusicApiService
    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var musicMapper: MusicMapper

    interface GetArtistsListCallback : BaseCallback {
        fun onSuccess(artists: ArrayList<Artist>)
    }

    interface GetAlbumsListCallback : BaseCallback {
        fun onSuccess(albums: ArrayList<Album>)
    }

    interface GetAlbumDetailsCallback : BaseCallback {
        fun onSuccess(album: Album)
    }

    /**
     * Get artists that match the given name
     */
    fun getArtists(
        artistName: String,
        page: Int,
        callback: GetArtistsListCallback){
        val request = object : GenericRequestHandler<SearchArtistJSON>(mContext) {

            override fun makeRequest(): Call<SearchArtistJSON> {
                return musicApiService.searchArtist(artistName, page)
            }

            override fun onSuccess(data: SearchArtistJSON) {
                callback.onSuccess(musicMapper.toArtistList(data))
            }

            override fun onFailure(error: MessageWrapper) {
                callback.onTechnicalError(error)
            }

        }

        request.doRequest(callback)

    }

    /**
     * Get the albums of an given artist
     */
    fun getAlbums(
        artistName: String,
        page: Int,
        callback: GetAlbumsListCallback){
        val request = object : GenericRequestHandler<GetTopAlbumsJSON>(mContext) {

            override fun makeRequest(): Call<GetTopAlbumsJSON> {
                return musicApiService.searchAlbums(artistName, page)
            }

            override fun onSuccess(data: GetTopAlbumsJSON) {
                callback.onSuccess(musicMapper.toAlbumList(data))
            }

            override fun onFailure(error: MessageWrapper) {
                callback.onTechnicalError(error)
            }

        }

        request.doRequest(callback)

    }

    /**
     * Get Album details
     */
    fun getAlbumDetails(
        artistName: String,
        albumName: String,
        callback: GetAlbumDetailsCallback){
        val request = object : GenericRequestHandler<GetAlbumInfoJSON>(mContext) {

            override fun makeRequest(): Call<GetAlbumInfoJSON> {
                return musicApiService.getAlbumDetails(artistName, albumName)
            }

            override fun onSuccess(data: GetAlbumInfoJSON) {
                musicMapper.toAlbum(data)?.let { callback.onSuccess(it) }
            }

            override fun onFailure(error: MessageWrapper) {
                callback.onTechnicalError(error)
            }

        }

        request.doRequest(callback)

    }


}