package com.lamrani.application.albums.domain.model


/**
 * Created by Lamrani Othmane on 2020-07-09.
 */
data class Artist(
    val image: String,
    val listeners: String,
    val mbid: String,
    val name: String,
    val streamable: String,
    val url: String
)