package com.lamrani.application.albums.app.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lamrani.application.R
import com.lamrani.application.albums.app.adapter.TrackAdapter
import com.lamrani.application.albums.app.viewModel.GetAlbumDetailsViewModel
import com.lamrani.application.albums.di.MusicInjector
import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.core.app.fragment.BaseFragment
import com.lamrani.application.core.app.wrapper.MessageType
import com.lamrani.application.core.app.wrapper.MessageWrapper
import com.lamrani.application.core.utils.Utils
import kotlinx.android.synthetic.main.fragment_album_details.*
import javax.inject.Inject

class SearchAlbumDetailsFragment : BaseFragment() {

    @Inject
    lateinit var getAlbumDetailsViewModel: GetAlbumDetailsViewModel
    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var trackAdapter: TrackAdapter

    private var artistName: String? = null
    private var albumName: String? = null
    private var currentAlbum: Album? = null
    private var isSaved = false


    override fun onAttach(context: Context) {
        super.onAttach(context)
        MusicInjector.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getAlbumDetailsViewModel.messageLD.observe(this, Observer { handleMessage(it) })
        getAlbumDetailsViewModel.albumLD.observe(this, Observer { renderAlbum(it) })
        getAlbumDetailsViewModel.localAlbumLD.observe(this, Observer { handlELocalAlbumSaved(it) })
        getAlbumDetailsViewModel.addedAlbumLD.observe(this, Observer { onAlbumAdded(it) })
        getAlbumDetailsViewModel.removedAlbumLD.observe(this, Observer { onAlbumRemoved(it) })

        // getting the artist using safe args
        val args = arguments?.let { SearchAlbumDetailsFragmentArgs.fromBundle(it) }
        artistName =  args?.StringArgArtist
        albumName =  args?.StringArgAlbum
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_album_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getAlbumDetailsViewModel.findLocalAlbum(artistName, albumName)

        showLoader()

        // Init the recycler view
        // set the list adapter
        val mLayoutManager = LinearLayoutManager(mContext)
        mLayoutManager.orientation = RecyclerView.VERTICAL
        recyclerViewTracks.layoutManager = mLayoutManager
        recyclerViewTracks.isNestedScrollingEnabled = false
        recyclerViewTracks.itemAnimator = DefaultItemAnimator()
        // to add a simple line
        val itemDecor = DividerItemDecoration(mContext, RecyclerView.VERTICAL)
        recyclerViewTracks.addItemDecoration(itemDecor)
        // set the adapter
        trackAdapter.onClickListener = {
            Toast.makeText(mContext, it.name, Toast.LENGTH_SHORT).show()
        }
        recyclerViewTracks.adapter = trackAdapter

        getAlbumDetailsViewModel.getAlbumDetails(artistName, albumName)

        btn_action.setOnClickListener(ActionClickListener())

    }

    inner class ActionClickListener : View.OnClickListener {
        override fun onClick(v: View?) {
            currentAlbum?.let {
                if(isSaved){
                    btn_action.text = mContext.getText(R.string.btn_remove)
                    getAlbumDetailsViewModel.removeAlbum(it)
                }
                else{
                    btn_action.text = mContext.getText(R.string.btn_add)
                    getAlbumDetailsViewModel.addAlbum(it)
                }
            }
        }
    }

    private fun renderAlbum(album: Album) {
        this.currentAlbum = album
        // set Album data
        tv_name.text = album.name
        tvListeners.text =  mContext.getString(R.string.listeners, album.listeners?.let { Utils.withSuffixNumber(it.toLong()) })
        tvPlaycount.text = mContext.getString(R.string.played, Utils.withSuffixNumber(album.playcount.toLong()))
        tv_artist.text = album.artist
        // set image
        Glide.with(mContext)
            .load(album.image)
            .circleCrop()
            .error(R.drawable.ic_music_note_black_24dp)
            .into(iv_image)
        // Set the tracks
        trackAdapter.addAll(album.tracks)
        // hide the loader
        hideLoader()
    }

    private fun onAlbumAdded(id: Long){

        when(id){
            0.toLong() -> handleMessage(MessageWrapper(
                messageType = MessageType.ERROR,
                message = mContext.getString(R.string.removing_album_failed)
            ))
            else -> {
                btn_action.text = mContext.getText(R.string.btn_remove)
                isSaved = true
                handleMessage(MessageWrapper(
                    messageType = MessageType.SUCCESS,
                    message = mContext.getString(R.string.album_added_successfully) + " (Id: $id)"
                ))
            }
        }

    }

    private fun onAlbumRemoved(removed: Int){
        when(removed){
            0 -> handleMessage(MessageWrapper(
                messageType = MessageType.ERROR,
                message = mContext.getString(R.string.removing_album_failed)
            ))
            else -> {
                btn_action.text = mContext.getText(R.string.btn_add)
                isSaved = false
                handleMessage(MessageWrapper(
                    messageType = MessageType.SUCCESS,
                    message = mContext.getString(R.string.album_removed_successfully)
                ))
            }
        }
    }

    private fun handlELocalAlbumSaved(album: Album?){
        album?.also {
            isSaved = true
            btn_action.text = mContext.getText(R.string.btn_remove)
        } ?: run {
            isSaved = false
            btn_action.text = mContext.getText(R.string.btn_add)
        }

    }

}