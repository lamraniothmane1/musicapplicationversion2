package com.lamrani.application.albums.data.room.dao

import androidx.room.*
import com.lamrani.application.albums.data.room.entity.AlbumEntity
import com.lamrani.application.albums.data.room.entity.AlbumWithTracks
import com.lamrani.application.albums.domain.model.Track

/**
 * Created by Lamrani Othmane on 2020-07-11.
 */
@Dao
interface MusicDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAlbum(albumEntity: AlbumEntity) : Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTracks(tracks: List<Track>) : List<Long>


    @Query("SELECT * FROM ALBUM_TABLE WHERE name=:albumName AND artist=:artistName")
    fun getAlbumDetails(artistName: String, albumName: String) : AlbumWithTracks

    @Transaction
    @Query("SELECT * FROM ALBUM_TABLE")
    fun getAllAlbums() : List<AlbumWithTracks>

    @Query("DELETE  FROM ALBUM_TABLE WHERE name=:albumName AND artist=:artistName")
    fun deleteAlbum(albumName: String, artistName: String) : Int

}