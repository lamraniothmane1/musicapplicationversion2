package com.lamrani.application.albums.app.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lamrani.application.R
import com.lamrani.application.albums.app.adapter.ArtistAdapter
import com.lamrani.application.albums.app.listener.EndlessRecyclerViewScrollListener
import com.lamrani.application.albums.app.viewModel.SearchArtistViewModel
import com.lamrani.application.albums.di.MusicInjector
import com.lamrani.application.albums.domain.model.Artist
import com.lamrani.application.core.app.fragment.BaseFragment
import com.lamrani.application.core.utils.Utils
import kotlinx.android.synthetic.main.fragment_search_artists_list.*
import javax.inject.Inject

class SearchArtistListFragment : BaseFragment() {

    @Inject
    lateinit var searchArtistViewModel: SearchArtistViewModel
    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var artistAdapter: ArtistAdapter


    override fun onAttach(context: Context) {
        super.onAttach(context)
        MusicInjector.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchArtistViewModel.messageLD.observe(this, Observer { handleMessage(it) })
        searchArtistViewModel.artistsLD.observe(this, Observer { renderArtists(it) })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_artists_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // set the listener for search view
        searchView.searchListener = { query ->
            artistAdapter.clear()
            searchArtistViewModel.getArtists(query, 1)
        }

        // Init the recycler view
        // set the list adapter
        val mLayoutManager = LinearLayoutManager(mContext)
        mLayoutManager.orientation = RecyclerView.VERTICAL
        recyclerArtists.layoutManager = mLayoutManager
        recyclerArtists.itemAnimator = DefaultItemAnimator()
        // to add a simple line
        val itemDecor = DividerItemDecoration(mContext, RecyclerView.VERTICAL)
        recyclerArtists.addItemDecoration(itemDecor)
        // set the adapter
        artistAdapter.onClickListener = {
            activity?.let { a ->
                Utils.hideKeyBoard(a)
            }
            // set the argument through safe args
            navigateTo(
                action =  SearchArtistListFragmentDirections.actionNavigationSearchToNavigationSearchAlbums(it.name)
            )
        }
        recyclerArtists.adapter = artistAdapter

        // set listener for pagination
        recyclerArtists.addOnScrollListener(object : EndlessRecyclerViewScrollListener(mLayoutManager){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                showLoader()
                searchArtistViewModel.getArtists(searchView.text, page+1)
            }
        })

    }

    private fun renderArtists(list: ArrayList<Artist>) {
        if(list.isEmpty()){
            recyclerArtists.visibility = View.GONE
            tvNoResult.visibility = View.VISIBLE
        } else {
            recyclerArtists.visibility = View.VISIBLE
            tvNoResult.visibility = View.GONE
            artistAdapter.addAll(list)
        }
        hideLoader()
    }


}