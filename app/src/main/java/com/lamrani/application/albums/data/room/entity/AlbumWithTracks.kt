package com.lamrani.application.albums.data.room.entity

import androidx.room.Embedded
import androidx.room.Relation
import com.lamrani.application.albums.domain.model.Track

/**
 * Created by Lamrani Othmane on 2020-07-12.
 */
data class AlbumWithTracks(

    @Embedded
    val album: AlbumEntity,
    @Relation(
        parentColumn = "albumId",
        entityColumn = "trackAlbumId"
    )
    val tracks: List<Track>

)