package com.lamrani.application.albums.data.room.entity

import androidx.room.*

/**
 * Created by Lamrani Othmane on 2020-07-10.
 */
@Entity(tableName = "album_table")
data class AlbumEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "albumId")
    val id: Int? = null,
    @ColumnInfo(name = "artist")
    val artist: String,
    @ColumnInfo(name = "image")
    val image: String,
    @ColumnInfo(name = "mbid")
    val mbid: String,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "listeners")
    val listeners: String? = null,
    @ColumnInfo(name = "playcount")
    val playcount: String,
    @ColumnInfo(name = "url")
    val url: String
)