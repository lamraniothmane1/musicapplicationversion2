package com.lamrani.application.albums.app.viewModel

import androidx.lifecycle.MutableLiveData
import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.albums.domain.usecase.SearchAlbumsUseCase
import com.lamrani.application.core.app.callback.BaseViewModel
import com.lamrani.application.core.app.wrapper.MessageWrapper
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-10.
 */
class SearchAlbumsViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var searchAlbumsUseCase: SearchAlbumsUseCase

    var albumsLD = MutableLiveData<ArrayList<Album>>()


    fun getAlbums(artistName: String?, page: Int){
        artistName?.let {
            searchAlbumsUseCase.execute(
                artistName = it,
                page = page,
                callback = object : SearchAlbumsUseCase.Callback{
                    override fun onSuccess(albums: ArrayList<Album>) {
                        albumsLD.value = albums
                    }

                    override fun onTechnicalError(error: MessageWrapper) {
                        this@SearchAlbumsViewModel.onBaseTechnicalError(error)
                    }

                    override fun onNetworkUnavailable() {
                        this@SearchAlbumsViewModel.onBaseNetworkUnavailable()
                    }

                }
            )
        }
    }

}