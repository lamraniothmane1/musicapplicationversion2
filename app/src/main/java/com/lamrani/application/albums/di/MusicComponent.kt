package com.lamrani.application.albums.di

import com.lamrani.application.albums.app.fragment.SearchAlbumDetailsFragment
import com.lamrani.application.albums.app.fragment.SearchAlbumsListFragment
import com.lamrani.application.albums.app.fragment.SearchArtistListFragment
import com.lamrani.application.albums.app.fragment.UsersAlbumListFragment
import com.lamrani.application.core.di.application.AppComponent
import com.lamrani.application.core.di.scopes.ScopeFragment
import dagger.Component


@Component(dependencies = [AppComponent::class], modules = [MusicModule::class])
@ScopeFragment
interface MusicComponent {

    fun inject(searchArtistListFragment: SearchArtistListFragment)
    fun inject(searchAlbumsListFragment: SearchAlbumsListFragment)
    fun inject(usersAlbumListFragment: UsersAlbumListFragment)
    fun inject(searchAlbumDetailsFragment: SearchAlbumDetailsFragment)

}