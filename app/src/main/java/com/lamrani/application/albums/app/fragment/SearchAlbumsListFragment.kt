package com.lamrani.application.albums.app.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lamrani.application.R
import com.lamrani.application.albums.app.adapter.AlbumAdapter
import com.lamrani.application.albums.app.listener.EndlessRecyclerViewScrollListener
import com.lamrani.application.albums.app.viewModel.SearchAlbumsViewModel
import com.lamrani.application.albums.di.MusicInjector
import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.core.app.fragment.BaseFragment
import com.lamrani.application.core.utils.Utils
import kotlinx.android.synthetic.main.fragment_search_albums_list.*
import javax.inject.Inject

class SearchAlbumsListFragment : BaseFragment() {

    @Inject
    lateinit var searchAlbumsViewModel: SearchAlbumsViewModel
    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var albumAdapter: AlbumAdapter

    private var artistName: String? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        MusicInjector.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchAlbumsViewModel.messageLD.observe(this, Observer { handleMessage(it) })
        searchAlbumsViewModel.albumsLD.observe(this, Observer { renderAlbums(it) })

        // getting the artist using safe args
        val args = arguments?.let { SearchAlbumsListFragmentArgs.fromBundle(it) }
        artistName =  args?.StringArgArtist
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_search_albums_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showLoader()

        // Init the recycler view
        // set the list adapter
        val mLayoutManager = LinearLayoutManager(mContext)
        mLayoutManager.orientation = RecyclerView.VERTICAL
        recyclerAlbums.layoutManager = mLayoutManager
        recyclerAlbums.itemAnimator = DefaultItemAnimator()
        // to add a simple line
        val itemDecor = DividerItemDecoration(mContext, RecyclerView.VERTICAL)
        recyclerAlbums.addItemDecoration(itemDecor)
        // set the adapter
        albumAdapter.onClickListener = {
            activity?.let { a ->
                Utils.hideKeyBoard(a)
            }
            // set the argument through safe args
            navigateTo(
                action =  SearchAlbumsListFragmentDirections.actionNavigationSearchAlbumsToNavigationSearchAlbumDetails(
                    StringArgAlbum = it.name,
                    StringArgArtist = artistName
                )
            )
        }
        recyclerAlbums.adapter = albumAdapter

        // set listener for pagination
        recyclerAlbums.addOnScrollListener(object : EndlessRecyclerViewScrollListener(mLayoutManager){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                showLoader()
                searchAlbumsViewModel.getAlbums(artistName, page+1)
            }
        })

        searchAlbumsViewModel.getAlbums(artistName, 1)
    }

    private fun renderAlbums(list: ArrayList<Album>) {
        albumAdapter.addAll(list)
        hideLoader()
    }


}