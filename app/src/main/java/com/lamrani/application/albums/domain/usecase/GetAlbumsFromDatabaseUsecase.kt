package com.lamrani.application.albums.domain.usecase

import com.lamrani.application.albums.data.repository.MusicDatabaseRepository
import com.lamrani.application.albums.domain.mapper.MusicMapper
import com.lamrani.application.albums.domain.model.Album
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-11.
 */

class GetAlbumsFromDatabaseUsecase @Inject constructor() {

    @Inject
    lateinit var databaseRepository: MusicDatabaseRepository
    @Inject
    lateinit var musicMapper: MusicMapper

    fun execute() : ArrayList<Album> {
        return  ArrayList<Album>().apply {
            for (albumTrack in databaseRepository.getAllAlbums()){
                musicMapper.toAlbum(albumTrack)?.let { add(it) }
            }
        }
    }

}