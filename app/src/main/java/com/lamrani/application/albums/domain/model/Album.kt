package com.lamrani.application.albums.domain.model

/**
 * Created by Lamrani Othmane on 2020-07-10.
 */
data class Album(
    val artist: String,
    val image: String,
    val mbid: String,
    val name: String,
    val listeners: String? = null,
    val playcount: String,
    val url: String,
    val tracks: ArrayList<Track> = ArrayList()
)