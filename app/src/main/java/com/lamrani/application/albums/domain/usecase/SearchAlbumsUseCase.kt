package com.lamrani.application.albums.domain.usecase

import com.lamrani.application.albums.data.repository.MusicRepository
import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.core.app.callback.BaseCallback
import com.lamrani.application.core.app.wrapper.MessageWrapper
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-10.
 */
class SearchAlbumsUseCase @Inject constructor() {

    @Inject
    lateinit var musicRepository: MusicRepository

    interface Callback : BaseCallback {
        fun onSuccess(albums: ArrayList<Album>)
    }

    fun execute(artistName: String, page: Int, callback: Callback){
        musicRepository.getAlbums(
            artistName = artistName,
            page = page,
            callback = object : MusicRepository.GetAlbumsListCallback{
                override fun onSuccess(albums: ArrayList<Album>) {
                    callback.onSuccess(albums)
                }

                override fun onTechnicalError(error: MessageWrapper) {
                    callback.onTechnicalError(error)
                }

                override fun onNetworkUnavailable() {
                    callback.onNetworkUnavailable()
                }

            }
        )
    }

}