package com.lamrani.application.albums.app.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lamrani.application.R
import com.lamrani.application.albums.app.adapter.AlbumAdapter
import com.lamrani.application.albums.app.viewModel.GetSavedAlbumsViewModel
import com.lamrani.application.albums.di.MusicInjector
import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.core.app.fragment.BaseFragment
import com.lamrani.application.core.utils.Utils
import kotlinx.android.synthetic.main.fragment_users_album_list.*
import javax.inject.Inject

class UsersAlbumListFragment : BaseFragment() {

    @Inject
    lateinit var getSavedAlbumsViewModel: GetSavedAlbumsViewModel
    @Inject
    lateinit var albumAdapter: AlbumAdapter
    @Inject
    lateinit var mContext: Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        MusicInjector.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getSavedAlbumsViewModel.messageLD.observe(this, Observer { handleMessage(it) })
        getSavedAlbumsViewModel.albumsLD.observe(this, Observer { renderAlbums(it) })
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_users_album_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showLoader()

        // set the listener for search view
        searchView.searchListener = { query ->
            albumAdapter.filter.filter(query)
        }


        // Init the recycler view
        // set the list adapter
        val mLayoutManager = LinearLayoutManager(mContext)
        mLayoutManager.orientation = RecyclerView.VERTICAL
        recyclerAlbums.layoutManager = mLayoutManager
        recyclerAlbums.itemAnimator = DefaultItemAnimator()
        // to add a simple line
        val itemDecor = DividerItemDecoration(mContext, RecyclerView.VERTICAL)
        recyclerAlbums.addItemDecoration(itemDecor)
        // set the adapter
        albumAdapter.onClickListener = {
            activity?.let { a ->
                Utils.hideKeyBoard(a)
            }
            // set the argument through safe args
            navigateTo(
                action =  UsersAlbumListFragmentDirections.actionNavigationUsersAlbumsToNavigationSearchAlbumDetails(
                    StringArgAlbum = it.name,
                    StringArgArtist = it.artist
                )
            )
        }
        recyclerAlbums.adapter = albumAdapter

        getSavedAlbumsViewModel.getSavedAlbums()
    }

    private fun renderAlbums(list: ArrayList<Album>) {
        if(list.isEmpty()){
            recyclerAlbums.visibility = View.GONE
            tvNoResult.visibility = View.VISIBLE
        }
        else{
            recyclerAlbums.visibility = View.VISIBLE
            tvNoResult.visibility = View.GONE
            albumAdapter.clear()
            albumAdapter.addAll(list)
        }
        hideLoader()
    }


}