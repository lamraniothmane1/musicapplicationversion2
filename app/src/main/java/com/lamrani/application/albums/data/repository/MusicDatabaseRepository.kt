package com.lamrani.application.albums.data.repository

import com.lamrani.application.albums.data.room.dao.MusicDao
import com.lamrani.application.albums.data.room.entity.AlbumEntity
import com.lamrani.application.albums.data.room.entity.AlbumWithTracks
import com.lamrani.application.albums.domain.model.Track
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-11.
 */
class MusicDatabaseRepository @Inject constructor() {

    @Inject
    lateinit var musicDao: MusicDao

    fun addAlbum(album: AlbumEntity, tracks: ArrayList<Track>) : Long {
        val idAlbum = musicDao.insertAlbum(album)
        for(track in tracks){
            track.trackAlbumId = idAlbum.toInt()
        }
        musicDao.insertTracks(tracks.toList())
        return idAlbum
    }

    fun deleteAlbum(albumName: String, artistName: String) : Int=  musicDao.deleteAlbum(albumName, artistName)
    fun getAllAlbums() : List<AlbumWithTracks> = musicDao.getAllAlbums()
    fun getAlbumDetails(artistName: String, albumName: String) : AlbumWithTracks = musicDao.getAlbumDetails(artistName, albumName)


}