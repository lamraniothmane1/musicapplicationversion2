package com.lamrani.application.core.di.application

import android.content.Context
import com.lamrani.application.MainActivity
import com.lamrani.application.core.di.retrofit.RetrofitModule
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Component(modules = [AppModule::class, RetrofitModule::class])
@Singleton
interface AppComponent {

    fun exposeContext(): Context
    fun exposeRetrofit(): Retrofit

    fun inject(mainActivity: MainActivity)

}
