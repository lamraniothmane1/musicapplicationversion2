package com.lamrani.application.core.app.views

import android.app.Activity
import android.content.Context
import androidx.core.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import com.lamrani.application.R
import com.lamrani.application.core.utils.Utils
import kotlinx.android.synthetic.main.custom_search_view.view.*


class MySearchView : RelativeLayout {

    private var drawableIconSearch: Int = 0
    private var drawableIconCancel: Int = 0
    private var mContext: Context
    var searchListener: ( (query: String) -> Unit )? = null
    private val searchEmptyMessage = "You should implement: setSearchListener method for MySearchView"
    val text : String  get() = search_text_view.text.toString()

    constructor(context: Context) : super(context) {
        this.mContext = context
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.mContext = context
        initAttributes(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        this.mContext = context
        initAttributes(attrs)
    }

    private fun initAttributes(attrs: AttributeSet) {

        View.inflate(context, R.layout.custom_search_view, this)


        val typedArray = this.mContext.obtainStyledAttributes(attrs, R.styleable.MySearchView, 0, 0)

        // set the hint
        val hint = typedArray.getString(R.styleable.MySearchView_hint_search_view)
        if (hint != null) search_text_view?.hint = hint


        // set the search icon
        drawableIconSearch = typedArray.getInt(R.styleable.MySearchView_search_icon, R.drawable.ic_search_black_24dp)
        filterTextImg!!.setImageDrawable(ContextCompat.getDrawable(mContext, drawableIconSearch))

        // set the cancel icon
        drawableIconCancel =
            typedArray.getInt(R.styleable.MySearchView_cancel_icon, R.drawable.ic_backspace_black_24dp)
        filterTextImg?.setImageDrawable(ContextCompat.getDrawable(mContext, drawableIconCancel))

        initEditText()

        typedArray.recycle()

    }

    private fun clearText(): Boolean {
        search_text_view.setText("")
        searchListener?.invoke("")
        return true
    }

    private fun initEditText() {
        val filterTextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                val editable = search_text_view.text
                if (editable != null) {
                    val query = search_text_view.text.toString()
                    if (query.isEmpty()) {
                        filterTextImg?.setImageResource(drawableIconSearch)
                    } else {
                        filterTextImg?.setImageResource(drawableIconCancel)
                    }
                    searchListener?.invoke(query)
                }
            }
        }


        search_text_view.addTextChangedListener(filterTextWatcher)
        search_text_view.setOnKeyListener { _, keyCode, event ->
            // If the event is a key-down event on the "enter" button
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                search()
            }
            false
        }

        filterTextImg?.setOnClickListener { clearText() }
        filterTextImg?.setOnLongClickListener { clearText() }

    }

    private fun search() {
        val query = text
        if (searchListener == null) {
            mContext.let { Toast.makeText(it, searchEmptyMessage, Toast.LENGTH_SHORT).show() }
        } else {
            searchListener?.invoke(query)
        }
        Utils.hideKeyBoard((mContext as Activity?)!!)
    }


}
