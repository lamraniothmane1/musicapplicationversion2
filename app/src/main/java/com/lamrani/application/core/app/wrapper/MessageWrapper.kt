package com.lamrani.application.core.app.wrapper

data class MessageWrapper constructor(
    var code : Int? = null,
    var message : String,
    var messageType: MessageType
)