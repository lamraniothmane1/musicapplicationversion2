package com.lamrani.application.core.utils

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.view.inputmethod.InputMethodManager
import kotlin.math.ln
import kotlin.math.pow


object Utils {


    fun hideKeyBoard(activity: Activity) {
        val view = activity.currentFocus
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    fun withSuffixNumber(count: Long): String? {
        if (count < 1000) return "" + count
        val exp = (ln(count.toDouble()) / ln(1000.0)).toInt()
        return String.format(
            "%.1f %c",
            count / 1000.0.pow(exp.toDouble()),
            "kMGTPE"[exp - 1]
        )
    }

    fun secondsToTime(seconds: String) : String {
        val duration: Int = Integer.valueOf(seconds)
        val s = duration % 60
        val minute = duration / 60
        val m = minute % 60
        val h = minute / 60
        return if (h == 0) {
            String.format("%02d:%02d", m, s)
        } else String.format("%02d:%02d:%02d", h, m, s)
    }
}
