package com.lamrani.application.core.app.callback

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lamrani.application.core.app.wrapper.MessageType
import com.lamrani.application.core.app.wrapper.MessageWrapper

abstract class BaseViewModel : ViewModel(){

    var messageLD: MutableLiveData<MessageWrapper> = MutableLiveData()

    fun onBaseTechnicalError(error: MessageWrapper) {
        messageLD.value = error
    }

    fun onBaseNetworkUnavailable() {
        messageLD.value = MessageWrapper(0, "No Internet connection", MessageType.ERROR)
    }

}