package com.lamrani.application.core.di.application

import android.content.Context
import com.lamrani.application.MainActivity
import com.lamrani.application.core.di.retrofit.RetrofitModule

object AppInjector {

    fun buildComponent(context: Context): AppComponent {
        return DaggerAppComponent.builder()
            .appModule(AppModule(context))
            .retrofitModule(RetrofitModule())
            .build()
    }

    fun inject(mainActivity: MainActivity) {
        buildComponent(mainActivity).inject(mainActivity)
    }

}
