package com.lamrani.application.core.di.scopes

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ScopeFragment