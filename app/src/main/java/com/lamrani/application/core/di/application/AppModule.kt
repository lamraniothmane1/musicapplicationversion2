package com.lamrani.application.core.di.application

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(var appContext: Context) {

    @Provides
    @Singleton
    internal fun providesContext(): Context {
        return appContext
    }


}
