package com.lamrani.application.core.app.callback

import com.lamrani.application.core.app.wrapper.MessageWrapper


interface  BaseCallback {

    /**
     * When the API call is successful (code 200)
     */
    //fun onSuccess(data: )

    /**
     * When the API call fails (depending on the error code)
     */
    fun onTechnicalError(error : MessageWrapper)

    /**
     * When the network is not available
     */
    fun onNetworkUnavailable()

}