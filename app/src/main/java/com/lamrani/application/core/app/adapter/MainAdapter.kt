package com.lamrani.application.core.app.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.lamrani.application.R
import java.util.*

/**
 * Created by Lamrani Othmane on 2020-07-10.
 */
abstract class MainAdapter<T> :
    RecyclerView.Adapter<RecyclerView.ViewHolder?>(),
    Filterable {

    private var nonFilteredData: ArrayList<T> = ArrayList()
    private var mData: ArrayList<T> = ArrayList()
    var onClickListener: ((item: T) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MainEmptyViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_empty, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

    abstract fun compare(query: String, data: T): Boolean

    override fun getItemCount(): Int {
        return mData.size
    }

    fun getItemAtPosition(position: Int): T {
        return mData[position]
    }

    fun addAll(items: ArrayList<T>) {
        mData.addAll(items)
        nonFilteredData.addAll(items)
        notifyDataSetChanged()
    }

    fun addItem(item: T) {
        mData.add(item)
        nonFilteredData.add(item)
        notifyDataSetChanged()
    }

    fun removeItem(item: T) {
        mData.remove(item)
        nonFilteredData.remove(item)
        notifyDataSetChanged()
    }

    fun isEmpty() : Boolean = mData.isEmpty()

    fun clear() {
        mData.clear()
        nonFilteredData.clear()
        notifyDataSetChanged()
    }


    override fun getFilter(): Filter {
        return   object : Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                mData.clear()
                if (charSearch.isBlank()) {
                    mData.addAll(nonFilteredData)
                } else {
                    for (row in nonFilteredData) {
                        if (compare(charSearch, row)) {
                            mData.add(row)
                        }
                    }
                }
                val filterResults = FilterResults()
                filterResults.values = mData
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                notifyDataSetChanged()
            }

        }
    }


    /**
     * Main empty holder
     */
    inner class MainEmptyViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView)

}
