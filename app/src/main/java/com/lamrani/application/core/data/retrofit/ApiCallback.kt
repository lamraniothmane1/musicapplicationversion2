package com.lamrani.application.core.data.retrofit

import android.util.Log
import com.google.gson.GsonBuilder
import com.lamrani.application.core.app.wrapper.MessageType
import com.lamrani.application.core.app.wrapper.MessageWrapper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


abstract class ApiCallback<T> : Callback<T> {

    override fun onResponse(call: Call<T>, response: Response<T>) {
        response.body()?.also {

            Log.i("Retrofit", "*****************************")
            Log.i("Retrofit", "************** Retrofit response ***************")
            val gson = GsonBuilder().setPrettyPrinting().create() // pretty print
            val responseJSON = gson.toJson(response.body())
            Log.i("Retrofit", responseJSON)
            Log.i("Retrofit", "*****************************")

            if (response.code() == 200) {
                handleResponseSuccess(response.body())
            } else {
                handleTechnicalError(
                    MessageWrapper(
                        response.code(),
                        response.message(),
                        MessageType.ERROR
                    )
                )
            }
        } ?: run {
            handleTechnicalError(
                MessageWrapper(
                    response.code(),
                    response.message(),
                    MessageType.ERROR
                )
            )
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        var errorWrapper  = MessageWrapper(
            -1,
            "A technical failure has occurred, please check " + ApiCallback::class.java.name + " for more details",
            MessageType.ERROR
        )
        t.let {
            t.message!!.let {
                errorWrapper.message = t.message!!
            }
        }
        handleTechnicalError(errorWrapper)
    }

    protected abstract fun handleResponseSuccess(data: T)

    protected abstract fun handleTechnicalError(messageWrapper: MessageWrapper)

}