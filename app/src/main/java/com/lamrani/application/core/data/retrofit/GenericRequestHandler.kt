package com.lamrani.application.core.data.retrofit

import android.content.Context
import com.lamrani.application.core.app.callback.BaseCallback
import com.lamrani.application.core.app.wrapper.MessageWrapper
import com.lamrani.application.core.utils.Utils
import retrofit2.Call

abstract class GenericRequestHandler<R>(internal var context: Context) {

    /**
     * This method is abstract because it is specific for each use of this class
     * @return the Retrofit call from a given service
     */
    protected abstract fun makeRequest(): Call<R>

    protected abstract fun onSuccess(data: R)

    protected abstract fun onFailure(error: MessageWrapper)


    /**
     *
     * @param callback : Retrofit Network Callback
     * @return the Api callback with both methods handleSuccess and handleFailure
     */
    protected open fun apiCallback(callback: BaseCallback): ApiCallback<R> {
        return object : ApiCallback<R>() {
            override fun handleResponseSuccess(data: R) {
                onSuccess(data)
            }

            override fun handleTechnicalError(messageWrapper: MessageWrapper) {
                onFailure(messageWrapper)
            }
        }
    }

    /**
     * Execute the Retrofit call to the given web service
     * @param callback Retrofit Network Callback
     */
    fun doRequest(callback: BaseCallback) {
        // We first check if the network is available
        if (Utils.isNetworkAvailable(context)) {
            // retrofit call execution
            makeRequest().enqueue(apiCallback(callback))
        } else {
            callback.onNetworkUnavailable()
        }
    }

}