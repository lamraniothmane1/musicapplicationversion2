package com.lamrani.application.core

import android.app.Application
import com.lamrani.application.core.di.application.AppComponent
import com.lamrani.application.core.di.application.AppInjector


class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = AppInjector.buildComponent(this)
    }


}
