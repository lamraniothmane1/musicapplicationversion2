package com.lamrani.application.core.app.wrapper

/**
 * Created by Lamrani Othmane on 2020-07-09.
 */

enum class MessageType{
    SUCCESS,
    ERROR
}