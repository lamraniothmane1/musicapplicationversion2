package com.lamrani.application.core.app.fragment

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import com.lamrani.application.R
import com.lamrani.application.core.app.wrapper.MessageType
import com.lamrani.application.core.app.wrapper.MessageWrapper

abstract class BaseFragment : Fragment() {

    private lateinit var mActivity: Activity
    private lateinit var mView: View
    private var navController: NavController? = null
    private val dialog : ProgressDialog by lazy {
        ProgressDialog(mActivity).apply {
            setMessage("loading")
            setCancelable(false)
            show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = requireActivity()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        mView = view
    }

    open fun handleMessage(messageWrapper: MessageWrapper) {
        hideLoader()
        val builder = AlertDialog.Builder(mActivity).apply {
            setCancelable(true)
            setMessage(messageWrapper.message)
            when(messageWrapper.messageType){
                MessageType.ERROR -> {
                    setTitle(R.string.warning)
                    setIcon(R.drawable.ic_error_black_24dp)
                }
                MessageType.SUCCESS -> {
                    setTitle(R.string.success)
                    setIcon(R.drawable.ic_check_circle_black_24dp)
                }
            }
            setPositiveButton(R.string.ok) { _, _ ->

            }
        }
        builder.show()

    }


    fun navigateTo(action: NavDirections){
        navController?.navigate(action)
    }


    /**
     * Shows a loader
     */
    protected open fun showLoader() {
        dialog.show()
    }

    /**
     * Hides the loader
     */
    protected open fun hideLoader() {
        dialog.dismiss()
    }



}
