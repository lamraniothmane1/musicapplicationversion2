package com.lamrani.application

import com.lamrani.application.albums.data.service.MusicApiService
import com.lamrani.application.albums.data.json.GetAlbumInfoJSON
import com.lamrani.application.albums.data.json.GetTopAlbumsJSON
import com.lamrani.application.albums.data.json.SearchArtistJSON
import com.lamrani.application.di.MusicTestInjector
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-12.
 */
class APITest {

    @Inject
    lateinit var musicApiService: MusicApiService

    @Before
    fun setUp(){
        MusicTestInjector.inject(this)
    }

    @Test
    fun check_searchArtists_call() {

        val call: Call<SearchArtistJSON> = musicApiService.searchArtist("Michael Jackson", 1)
        try {
            val response: Response<SearchArtistJSON> = call.execute()
            val artistResponse: SearchArtistJSON = response.body()
            assert(response.isSuccessful)
            assert(artistResponse.results?.artistmatches?.artist?.isEmpty()!!)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    @Test
    fun check_searchAlbums_call() {

        val call: Call<GetTopAlbumsJSON> = musicApiService.searchAlbums("The beatles", 1)
        try {
            val response: Response<GetTopAlbumsJSON> = call.execute()
            val artistResponse: GetTopAlbumsJSON = response.body()
            assert(response.isSuccessful)
            assert(artistResponse.topalbums?.album?.isEmpty()!!)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    @Test
    fun check_searchAlbumDetails_call() {

        val call: Call<GetAlbumInfoJSON> = musicApiService.getAlbumDetails("Michael Jackson", "Forever")
        try {
            val response: Response<GetAlbumInfoJSON> = call.execute()
            val artistResponse: GetAlbumInfoJSON = response.body()
            assert(response.isSuccessful)
            assertNotNull(artistResponse.album!!)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}