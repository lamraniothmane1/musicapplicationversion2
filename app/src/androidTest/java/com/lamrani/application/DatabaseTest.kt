package com.lamrani.application

import com.lamrani.application.albums.domain.model.Album
import com.lamrani.application.albums.domain.model.Track
import com.lamrani.application.albums.domain.usecase.AddAlbumToDatabaseUsecase
import com.lamrani.application.albums.domain.usecase.GetAlbumDetailsFromDatabaseUsecase
import com.lamrani.application.albums.domain.usecase.RemoveAlbumFromDatabaseUsecase
import com.lamrani.application.di.MusicTestInjector
import junit.framework.Assert.*
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

/**
 * Created by Lamrani Othmane on 2020-07-12.
 */
class DatabaseTest {

    @Inject
    lateinit var addAlbumToDatabaseUsecase: AddAlbumToDatabaseUsecase
    @Inject
    lateinit var getAlbumDetailsFromDatabaseUsecase: GetAlbumDetailsFromDatabaseUsecase
    @Inject
    lateinit var removeAlbumFromDatabaseUsecase: RemoveAlbumFromDatabaseUsecase

    private val albumAdd by lazy {
        Album(
            mbid = "MBID",
            name = "Diamonds",
            artist = "Ben Harper",
            image = "url",
            playcount = "2232323",
            url = "DSDSD",
            listeners = "232323",
            tracks = ArrayList<Track>().apply {
                add((Track(
                    artist = "Arist",
                    duration = "12:00",
                    name = "Track"
                )))
                add((Track(
                    artist = "Arist",
                    duration = "12:00",
                    name = "Track2"
                )))
            }
        )
    }

    private val albumDelete by lazy {
        Album(
            mbid = "MBID",
            name = "Burn to shine",
            artist = "Ben Harper",
            image = "url",
            playcount = "2232323",
            url = "DSDSD",
            listeners = "232323",
            tracks = ArrayList<Track>().apply {
                add((Track(
                    artist = "Arist",
                    duration = "12:00",
                    name = "Track"
                )))
                add((Track(
                    artist = "Arist",
                    duration = "12:00",
                    name = "Track2"
                )))
            }
        )
    }

    @Before
    fun createDb() {
        MusicTestInjector.inject(this)
    }

    @Test
    @Throws(Exception::class)
    fun addAlbumTest() {

        // save the new album
        val insertedID = addAlbumToDatabaseUsecase.execute(albumAdd)
        assertNotEquals(insertedID, 0)

        // we remove the album
        // get the saved album
        val savedItem = getAlbumDetailsFromDatabaseUsecase.execute(albumName = albumAdd.name, artistName = albumAdd.artist)
        // check if the saved item is not null
        assertNotNull(savedItem)

        // assert the values are correct
        assertEquals(savedItem?.name, albumAdd.name)

    }

    @Test
    @Throws(Exception::class)
    fun deleteAlbumTest() {

        // save the new album
        val insertedID = addAlbumToDatabaseUsecase.execute(albumDelete)
        assertNotEquals(insertedID, 0)

        // we remove the album
        // get the saved album
        var savedItem = getAlbumDetailsFromDatabaseUsecase.execute(albumName = albumDelete.name, artistName = albumDelete.artist)
        // check if the saved item is not null
        assertNotNull(savedItem)

        // assert the values are correct
        assertEquals(savedItem?.name, albumDelete.name)

        // delete the album
        assertEquals(removeAlbumFromDatabaseUsecase.execute(savedItem!!), 1)

        // get the item again
        savedItem = getAlbumDetailsFromDatabaseUsecase.execute(albumName = albumDelete.name, artistName = albumDelete.artist)
        assertNull(savedItem)

    }


}