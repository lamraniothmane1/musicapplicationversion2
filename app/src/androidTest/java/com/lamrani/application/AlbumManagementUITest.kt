package com.lamrani.application

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.lamrani.application.utils.TestUtils
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Created by Lamrani Othmane on 2020-07-12.
 */
@RunWith(AndroidJUnit4::class)
class AlbumManagementUITest {

    private lateinit var appContext: Context

    @get:Rule
    var activityTestRule: ActivityTestRule<MainActivity> = object : ActivityTestRule<MainActivity>(MainActivity::class.java, false, false) {}


    @Before
    fun setUp() {
        appContext = InstrumentationRegistry.getInstrumentation().targetContext
        appContext.deleteDatabase("music_database")
        val intent = Intent(appContext, MainActivity::class.java)
        activityTestRule.launchActivity(intent)
    }

    /**
     * This test requires an internet connection
     */
    @Test
    fun searchArtistWithAlbumDetails() {

        Thread.sleep(200)
        // check if the home Title is displayed
        TestUtils.checkHeader(R.string.title_home)
        // click on search artists
        onView(withId(R.id.navigation_search_artists)).check(matches(isDisplayed())).perform(click())
        Thread.sleep(700)
        // check if the title is correctly displayed
        TestUtils.checkHeader(R.string.title_search_artists)
        // type the name of an artist
        onView(withId(R.id.search_text_view))
            .check(matches(isDisplayed()))
            .perform(
                typeText("Eminem")
            )
            .perform(pressImeActionButton())
        Thread.sleep(500)
        //validate


        // wait for the results to be displayed
        Thread.sleep(5000)
        // Check if the list contains the artist
        val matcherArtist = withChild(withText("Eminem"))
        // We click on the first item found
        onView(withId(R.id.recyclerArtists)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Thread.sleep(5000)
        // Checking the title
        TestUtils.checkHeader(R.string.title_search_albums)
        // Click the first album
        onView(withId(R.id.recyclerAlbums)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Thread.sleep(5000)
        // Checking the title
        TestUtils.checkHeader(R.string.title_search_album_details)
        // check that the artist is Correct
        onView(allOf(withId(R.id.tv_artist), withSubstring("Eminem")))
    }

    /**
     * This test requires an internet connection
     */
    @Test
    fun addAlbumToList() {
        searchArtistWithAlbumDetails()

        // click on the add button
        onView(withText(R.string.btn_add)).check(matches(isDisplayed())).perform(click())
        Thread.sleep(2000)
        // check if the popup is shown with success message
        onView(withSubstring(appContext.getString(R.string.album_added_successfully))).check(matches(isDisplayed()))
        onView(withText(R.string.success)).check(matches(isDisplayed()))
        // click on okay
        onView(withText(R.string.ok)).perform(click())
        Thread.sleep(500)

        // go to saved album list
        onView(withId(R.id.navigation_users_albums)).perform(click())
        Thread.sleep(2000)
        // check the title
        TestUtils.checkHeader(R.string.title_user_albums)
        // check if the album exists
        onView(withId(R.id.recyclerAlbums)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Thread.sleep(5000)
        // check if the remove button is displayed
        onView(withText(R.string.btn_remove)).check(matches(isDisplayed()))
    }

    /**
     * This test requires an internet connection
     */
    @Test
    fun removeAlbumFromList() {
        addAlbumToList()

        onView(withText(R.string.btn_remove)).perform(click())
        // check if the popup is shown with success message
        onView(withSubstring(appContext.getString(R.string.album_removed_successfully))).check(matches(isDisplayed()))
        onView(withText(R.string.success)).check(matches(isDisplayed()))
        // click on okay
        onView(withText(R.string.ok)).perform(click())
        Thread.sleep(500)

    }

}