package com.lamrani.application.utils

import android.widget.TextView
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.instanceOf

/**
 * Created by Lamrani Othmane on 2020-07-12.
 */
class TestUtils {
    companion object{

        fun checkHeader(title: Int){
            Espresso.onView(
                allOf(
                    instanceOf(TextView::class.java),
                    withParent(withResourceName("action_bar"))
                )
            ).check(matches(withText(title)))
        }
    }


}