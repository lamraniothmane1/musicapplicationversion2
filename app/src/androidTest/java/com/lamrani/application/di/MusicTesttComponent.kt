package com.lamrani.application.di

import com.lamrani.application.APITest
import com.lamrani.application.DatabaseTest
import com.lamrani.application.albums.di.MusicModule
import com.lamrani.application.core.di.application.AppComponent
import com.lamrani.application.core.di.scopes.ScopeFragment
import dagger.Component

/**
 * Created by Lamrani Othmane on 2020-07-12.
 */
@Component(dependencies = [AppComponent::class], modules = [MusicModule::class])
@ScopeFragment
interface MusicTesttComponent {

    fun inject(entityReadWriteTest: DatabaseTest)
    fun inject(apiTest: APITest)


}