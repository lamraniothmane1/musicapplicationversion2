package com.lamrani.application.di

import com.lamrani.application.APITest
import com.lamrani.application.DatabaseTest
import com.lamrani.application.core.App

/**
 * Created by Lamrani Othmane on 2020-07-12.
 */
object MusicTestInjector {

    private fun buildComponent(): MusicTesttComponent {
        return DaggerMusicTesttComponent.builder()
            .appComponent(App.appComponent)
            .musicModule(MusicTestModule())
            .build()
    }

    fun inject(databaseTest: DatabaseTest) {
        buildComponent().inject(databaseTest)
    }

    fun inject(apiTest: APITest) {
        buildComponent().inject(apiTest)
    }

}
